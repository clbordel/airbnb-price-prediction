# Information

This study aims to develop an algorithm which can predict Air BnB price listings in the New Orleans market. 

The model makes use of several publicly available factors of each unit listing, such as:

1. Number of rooms
2. Zip code
3. Number of bathrooms
4. etc.

The data on Air BnB rentals is publicly available and is source from Air BnB directly. The Data can be found at:

http://insideairbnb.com/get-the-data.htmil

# Notes

__methods based on the following work__:
https://www.dataquest.io/blog/machine-learning-tutorial/

__data pre-processing__:
Pre-processing methods were undertaken and are described through in-code comments
